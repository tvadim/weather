namespace Weather.Tests
{

    using BL;
    using FluentAssertions;
    using Xunit;

    public class WeatherServiceShould
    {
        [Fact(DisplayName = "WeatherService должен успешно получить текущую информацию о погоде")]
        public void SuccessGetCurrentInfo()
        {
            // Arrange
            var svc = new WeatherService();
            var expected = new WeatherEntry()
            {
                MaxTemperature = -12.2,
                MinTemperature = -17.0,
                WeatherState = WeatherState.Clean
            };

            // Act
            var actual = svc.GetCurrentInfo();
            
            // Assert
            actual.IsSuccess.Should().BeTrue();
            actual.Value.Should().Be(expected);
        }
    }
}
