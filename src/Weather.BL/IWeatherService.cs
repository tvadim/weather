﻿namespace Weather.BL
{

	using CSharpFunctionalExtensions;

	public interface IWeatherService
	{
		Result<WeatherEntry> GetCurrentInfo();
	}

}