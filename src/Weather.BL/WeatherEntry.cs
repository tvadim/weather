namespace Weather.BL
{

	public record WeatherEntry
	{
		public double MaxTemperature { get; init; }
		public double MinTemperature { get; init; }
		public WeatherState WeatherState { get; init; }
	}

}