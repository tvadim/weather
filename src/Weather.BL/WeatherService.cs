﻿namespace Weather.BL
{

    using CSharpFunctionalExtensions;

    public class WeatherService : IWeatherService
    {
        public Result<WeatherEntry> GetCurrentInfo()
        {
            return new WeatherEntry()
            {
                MaxTemperature = -12.2,
                MinTemperature = -17.0,
                WeatherState = WeatherState.Clean
            };
        }
    }

}
