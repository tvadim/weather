namespace Weather.BL
{

	public enum WeatherState
	{
		Snow,
		Sleet,
		Hail,
		Clean
	}

}